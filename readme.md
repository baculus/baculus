# baculus

baculus is an attempt at designing for the [Mozilla / NSF wireless challenge][]

We are attempting to build a system to help people deal with the shock in the first 72 hours after a disaster where infrastructure is down.

## Goals

  * Connect people as quickly as possible
  * Provide power to cell phones in the first 12 hours
  * Help locate family and friends

## Notes

To access test mode for your phone, go into the dialer and type:

    *#*#4636#*#*

You can switch to GSM or CDMA and check what freq band you are

[Mozilla / NSF wireless challenge]: https://wirelesschallenge.mozilla.org/
